FROM debian:11-slim

RUN apt update
RUN apt install -y rolldice

ENTRYPOINT ["/usr/games/rolldice", "20"]
